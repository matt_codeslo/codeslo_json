var myObjects = [
    {
        food1:"apples",
        food2:"cheese",
        food3:"salami"
    },
    {
        car1:"BMW",
        car2:"Lexus",
        car3:"Tesla"
    },
    {
        book1:"Snow Crash",
        book2:"Seveneves",
        book3:"Cryptonomicon"
    }
];

// what would you expect to see from the following code:

console.log(myObjects[0].food1);

// how about this?

console.log(myObjects[2].book3);

// how about this?

console.log(myObjects[1][2]);

// how about this?

for(var i=0;i<myObjects.length;i++){
    var obj = myObjects[i];
    for(var key in obj){
        console.log(key+":"+obj[key]);
    }
}

