var myArr = [1,2,"apple","pie",true];

// CHALLENGES

// create a variable called 'firstElement' and make it equal to the first element of myArr

// create a variable called 'total' and make it equal to the sum of the first two elements of myArr

// create an if / else statement that returns 'yes' if the last element of myArr is true, or 'no' if it is false

// chance the last element of myArr to have a value of 'coffee'