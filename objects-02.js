var myObj = {
    firstValue:"foo",
    secondValue:"bar",
    thirdValue:"JavaScript"
};

// using a for-in loop, iterate through myObj and console.log all the keys

// using a for-in loop, iterate through myObj and console.log all the values

// using a for-in loop, iterate through myObj and console.log all key / value pairs in the format of "key:value"

// don't forget, in a loop it's bracket notation FTW!