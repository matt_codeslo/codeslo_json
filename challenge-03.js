var myObj = {
    firstArray:[
        {
            bandOne:'Pink Floyd',
            bandTwo:'Black Sabbath',
            bandThree:'Led Zeppelin'
        },
        {
            cityOne:'London',
            cityTwo:'Paris',
            cityThree:'Rome'
        }
    ]
};

// use iteration to console.log the key / value pairs of both elements in firstArray