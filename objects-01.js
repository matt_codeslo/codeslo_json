var myObj = {
    firstValue: 'foo',
    secondValue: 'bar',
    thirdValue: 'javascript'
};

var data = myObj.firstValue;
var moreData = myObj.secondValue;
var yetMoreData = myObj["thirdValue"];

// create a new variable that concatonates the first two properites of myObj

// change the third property of myObj to "JSON"

// add a fourth property called 'fourthValue' to myObj, and set its value to "Chrome"

// console.log your new property, twice, once using dot notation and once using bracket notation.

