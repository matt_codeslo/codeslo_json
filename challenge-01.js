var myObjects = [
    {
        food1:"apples",
        food2:"cheese",
        food3:"salami"
    },
    {
        car1:"BMW",
        car2:"Lexus",
        car3:"Tesla"
    },
    {
        book1:"Snow Crash",
        book2:"Seveneves",
        book3:"Cryptonomicon"
    }
];

// iterate through the myObjects array, and console.log all the key / value pairs from each element. Can you insert a space between each new element? 

